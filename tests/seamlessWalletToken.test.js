const { SeamlessWalletToken } = require('../index');
const { InvalidKeyOrSecretError, InvalidSeamlessPayloadError } = require('../src/errors');

let seamlessWalletToken;
const key = 'a778ec58-b9c2-419f-91dc-b0c1ed10873e';
const secret = '10b8ac75-4cac-4a77-a921-9a7c51ae1db6';
const payload = '{"requestId":"736b0822-baf0-4227-972d-a7b935129683","settleUserBalanceList":[{"balance":3830.0000,"currency":"NTD","user":"zaqx1994"}],"status":"ok"}';
const expectToken = 'dvzaj57uxbmbeo2escgmynmzihq=';

describe('test constructor method', () => {
    test('should be throw error if key is empty', () => {
        expect(() => {
            new SeamlessWalletToken('', 'secret');
        }).toThrow('The key or the secret should not be empty');

        expect(() => {
            new SeamlessWalletToken('', 'secret');
        }).toThrow(InvalidKeyOrSecretError);
    });

    test('should be throw error if secret is empty', () => {
        expect(() => {
            new SeamlessWalletToken('key', '');
        }).toThrow('The key or the secret should not be empty');

        expect(() => {
            new SeamlessWalletToken('key', '');
        }).toThrow(InvalidKeyOrSecretError);
    });
});

describe('test validate method', () => {
    beforeEach(() => {
        seamlessWalletToken = new SeamlessWalletToken(key, secret);
    });

    test('should be false if token is wrong', () => {
        expect(seamlessWalletToken.validate(payload, 'wrong-token')).toBeFalsy();
    });

    test('should be true if token is right', () => {
        expect(seamlessWalletToken.validate(payload, expectToken)).toBeTruthy();
    });
});

describe('test constructor method', () => {
    test('should be throw error if key is empty', () => {
        expect(() => {
            new SeamlessWalletToken('', 'secret');
        }).toThrow('The key or the secret should not be empty');

        expect(() => {
            new SeamlessWalletToken('', 'secret');
        }).toThrow(InvalidKeyOrSecretError);
    });
});

describe('test generateToken method', () => {
    beforeEach(() => {
        seamlessWalletToken = new SeamlessWalletToken(key, secret);
    });

    test('should be throw error if payload has no 4th decimal place', () => {
        let payload = '{"requestId":"736b0822-baf0-4227-972d-a7b935129683","settleUserBalanceList":[{"balance":3830,"currency":"NTD","user":"zaqx1994"}],"status":"ok"}';
        expect(() => {seamlessWalletToken.generateToken(payload);}).toThrow('The format error of payload');
        expect(() => {seamlessWalletToken.generateToken(payload);}).toThrow(InvalidSeamlessPayloadError);
    });

    test('should be throw error if payload is no alphabetical order', () => {
        let payload = '{"status":"ok", "requestId":"736b0822-baf0-4227-972d-a7b935129683","settleUserBalanceList":[{"balance":3830.0000,"currency":"NTD","user":"zaqx1994"}]}';
        expect(() => {seamlessWalletToken.generateToken(payload);}).toThrow('The format error of payload');
        expect(() => {seamlessWalletToken.generateToken(payload);}).toThrow(InvalidSeamlessPayloadError);
    });

    test('should be throw error if payload is no alphabetical order in settleUserBalanceList', () => {
        let payload = '{"requestId":"736b0822-baf0-4227-972d-a7b935129683","settleUserBalanceList":[{"currency":"NTD","balance":3830.0000,"user":"zaqx1994"}],"status":"ok"}';
        expect(() => {seamlessWalletToken.generateToken(payload);}).toThrow('The format error of payload');
        expect(() => {seamlessWalletToken.generateToken(payload);}).toThrow(InvalidSeamlessPayloadError);
    });

    test('token should be expected', () => {
        expect(seamlessWalletToken.generateToken(payload)).toEqual(expectToken);
    });
});

describe('test reformatPayload method', () => {
    beforeEach(() => {
        seamlessWalletToken = new SeamlessWalletToken('key', 'secret');
    });
    test('the number is less than 4th decimal place case', () => {
        expect(seamlessWalletToken.reformatPayload('{"balance":100.1}'))
            .toEqual('{"balance":100.1000}');
    });

    test('number has 5th decimal place case', () => {
        expect(seamlessWalletToken.reformatPayload('{"balance":100.12345}'))
            .toEqual('{"balance":100.1235}');
    });

    test('alphabetical order case for key', () => {
        expect(seamlessWalletToken.reformatPayload('{"c":100.12345,"a":"test","b":true}'))
            .toEqual('{"a":"test","b":true,"c":100.1235}');
    });

    test('alphabetical order case if value is object array', () => {
        expect(seamlessWalletToken.reformatPayload('{"list":[{"c":100.12345,"a":"test","b":true}]}'))
            .toEqual('{"list":[{"a":"test","b":true,"c":100.1235}]}');
    });

    test('do not add escaped slashes to time format', () => {
        expect(seamlessWalletToken.reformatPayload('{"betTime":"2022/01/25 13:35:59"}'))
            .toEqual('{"betTime":"2022/01/25 13:35:59"}');
    });

    test('do not reformat to 4th decimal place number when key = run, round, wagerId', () => {
        expect(seamlessWalletToken.reformatPayload('{"run":4,"round":99,"wagerId":1000,"others":10}'))
            .toEqual('{"others":10.0000,"round":99,"run":4,"wagerId":1000}');
    });

    test('release all number labels', () => {
        expect(seamlessWalletToken
            .reformatPayload('{"settleUserBalanceList":[{"balance":2},{"balance":2},{"balance":2}]}'))
            .toEqual('{"settleUserBalanceList":[{"balance":2.0000},{"balance":2.0000},{"balance":2.0000}]}');
    });
});
