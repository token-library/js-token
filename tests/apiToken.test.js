const md5 = require('crypto-js/md5');
const { DateTime } = require('luxon');

const { ApiToken } = require('../index');
const { InvalidKeyOrSecretError } = require('../src/errors');

describe('test constructor method', () => {
    test('should be throw error if key is empty', () => {
        expect(() => {
            new ApiToken('', 'secret');
        }).toThrow('The key or the secret should not be empty');

        expect(() => {
            new ApiToken('', 'secret');
        }).toThrow(InvalidKeyOrSecretError);
    });

    test('should be throw error if secret is empty', () => {
        expect(() => {
            new ApiToken('key', '');
        }).toThrow('The key or the secret should not be empty');

        expect(() => {
            new ApiToken('key', '');
        }).toThrow(InvalidKeyOrSecretError);
    });
});

describe('test generateKey method', () => {
    test('key should be expected', () => {
        const apiToken = new ApiToken('test-key', 'test-secret');
        const payload = '{"key1":"value","key2":true,"key3":false,"key4":12.3}';
        const key = apiToken.generateKey(payload);
        const hash = key.substr(6).substr(0, key.length - 12);

        const date = DateTime.now().setZone('UTC-4').toFormat('yyMMd');
        const keyG = md5(date + 'test-key' + 'test-secret').toString();
        const expectHash = md5('key1=value&key2=true&key3=false&key4=12.3' + keyG).toString();
        expect(hash).toEqual(expectHash);
    });
});
