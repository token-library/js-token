const { DateTime } = require('luxon');

const { ApiV2Signature } = require('../index');
const { InvalidKeyOrSecretError, InvalidRequestTimestampError } = require('../src/errors');

describe('test constructor method', () => {
    test('should be throw error if key is empty', () => {
        expect(() => {
            new ApiV2Signature('', 'secret');
        }).toThrow('The key or the secret should not be empty');

        expect(() => {
            new ApiV2Signature('', 'secret');
        }).toThrow(InvalidKeyOrSecretError);
    });

    test('should be throw error if secret is empty', () => {
        expect(() => {
            new ApiV2Signature('key', '');
        }).toThrow('The key or the secret should not be empty');

        expect(() => {
            new ApiV2Signature('key', '');
        }).toThrow(InvalidKeyOrSecretError);
    });
});

describe('test generateSignature method', () => {
    test('should be throw error if timestamp is over 15 mins', () => {
        expect(() => {
            const apiV2Signature = new ApiV2Signature('test-key', 'test-secret');
            apiV2Signature.generateSignature({}, DateTime.now().toUnixInteger() - (15 * 60 + 1));
        }).toThrow('The request unix timestamp is invalid');

        expect(() => {
            const apiV2Signature = new ApiV2Signature('test-key', 'test-secret');
            apiV2Signature.generateSignature({}, DateTime.now().toUnixInteger() - (15 * 60 + 1));
        }).toThrow(InvalidRequestTimestampError);
    });

    test('the signature is correct', () => {
        const apiV2Signature = new ApiV2Signature('test-key', 'test-secret');
        const payload = '{"key1":"value","key2":true,"key3":false,"key4":12.3,"key5":"https://test.com"}';
        const signature = 'equAw0DN1WWJxcM8OBd53/RfIpkRdhkhujjXa/6r+xs=';
        expect(apiV2Signature.validate(payload, 9876543210, signature)).toBeTruthy();
    });

    test('the signature is wrong', () => {
        const apiV2Signature = new ApiV2Signature('test-key', 'test-secret');
        const payload = '{"key1":"value","key2":true,"key3":false,"key4":12.3, "key5":"https://test.com"}';
        const signature = 'wrong-signature';
        expect(apiV2Signature.validate(payload, DateTime.now().toUnixInteger(), signature)).toBeFalsy();
    });
});
