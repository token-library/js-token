class InvalidKeyOrSecretError extends Error
{
    constructor(errorMessage)
    {
        super(errorMessage);
    }
}

class InvalidSeamlessPayloadError extends Error
{
    constructor(errorMessage)
    {
        super(errorMessage);
    }
}

class InvalidRequestTimestampError extends Error
{
    constructor(errorMessage)
    {
        super(errorMessage);
    }
}

module.exports = {
    InvalidKeyOrSecretError,
    InvalidSeamlessPayloadError,
    InvalidRequestTimestampError
};

