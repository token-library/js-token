const md5 = require('crypto-js/md5');
const { DateTime } = require('luxon');

const { InvalidKeyOrSecretError } = require('./errors');

module.exports = class ApiToken
{
    constructor(key, secret)
    {
        if (!key || !secret) {
            throw new InvalidKeyOrSecretError('The key or the secret should not be empty');
        }
        this.key = key;
        this.secret = secret;
    }

    generateKey(payload)
    {
        let params = JSON.parse(payload);

        let paramsString = '';
        for (const [key, value] of Object.entries(params)) {
            if (paramsString !== '') {
                paramsString += '&';
            }
            paramsString += key + '=' + value;
        }
        return this.getRandomString() + md5(paramsString + this.getKeyG()).toString() + this.getRandomString();
    }

    getKeyG()
    {
        let date = DateTime.now().setZone('UTC-4').toFormat('yyMMd');
        return md5(date + this.key + this.secret).toString();
    }

    getRandomString()
    {
        return Math.random().toString(36).substr(2,6);
    }
};
