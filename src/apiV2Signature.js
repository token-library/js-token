const base64 = require('crypto-js/enc-base64');
const hmacSha256 = require('crypto-js/hmac-sha256');
const { DateTime } = require('luxon');

const { InvalidKeyOrSecretError, InvalidRequestTimestampError } = require('./errors');

module.exports = class ApiToken
{
    constructor(key, secret)
    {
        if (!key || !secret) {
            throw new InvalidKeyOrSecretError('The key or the secret should not be empty');
        }
        this.key = key;
        this.secret = secret;
    }

    generateSignature(payload, timestamp)
    {
        if (timestamp < DateTime.now().toUnixInteger() - 15 * 60) {
            throw new InvalidRequestTimestampError('The request unix timestamp is invalid');
        }

        const message = this.key + payload + timestamp;
        return base64.stringify(hmacSha256(message, this.secret));
    }

    validate(payload, timestamp, signature)
    {
        const expectSignature = this.generateSignature(payload, timestamp);
        return expectSignature === signature;
    }
};
