const base64 = require('crypto-js/enc-base64');
const hex = require('crypto-js/enc-hex');
const sha1 = require('crypto-js/sha1');

const { InvalidKeyOrSecretError, InvalidSeamlessPayloadError } = require('./errors');

module.exports = class SeamlessWalletToken
{
    constructor(key, secret)
    {
        if (!key || !secret) {
            throw new InvalidKeyOrSecretError('The key or the secret should not be empty');
        }
        this.key = key ;
        this.secret = secret;
    }

    validate(payload, token)
    {
        let expectToken = this.generateToken(payload);
        return expectToken === token;
    }

    generateToken(payload)
    {
        if (payload !== this.reformatPayload(payload)) {
            throw new InvalidSeamlessPayloadError('The format error of payload');
        }

        return base64.stringify(hex.parse(sha1(this.key + payload + this.secret).toString())).toLowerCase();
    }

    reformatPayload(payload)
    {
        let params = JSON.parse(payload);
        params = this.handleReformation(params);

        //remove labels from number("%100.0000^" => 100.0000)
        return JSON.stringify(params).replace(/\^"/g, '').replace(/"%/g, '');
    }

    handleReformation(params)
    {
        let sortParams;
        if (Array.isArray(params)) {
            sortParams = params.sort();
        } else {
            sortParams = Object.keys(params).sort().reduce(
                (obj, key) => {
                    obj[key] = params[key];
                    return obj;
                },
                {}
            );
        }

        for (const [key, value] of Object.entries(sortParams)) {
            if (Array.isArray(value) || (typeof value === 'object' && value !== null)) {
                sortParams[key] = this.handleReformation(value);
            }

            if(typeof value === 'number' && !['run', 'round', 'wagerId'].includes(key)) {
                let newNumber = value.toFixed(4);

                //add labels to number("100.0000" => "%100.0000^")
                sortParams[key] = '%' + newNumber + '^';
            }
        }

        return sortParams;
    }
};
