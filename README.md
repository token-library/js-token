# JS-TOKEN

A simple library to validate and generate token in Node.js

## Installation

- npm
```
npm config set @token-library:registry https://gitlab.com/api/v4/projects/35343364/packages/npm/
npm install @token-library/js-token
```

- yarn
```
yarn config set @token-library:registry https://gitlab.com/api/v4/projects/35343364/packages/npm/
yarn add @token-library/js-token
```

## Example

Running in Node.js

### Seamless 1.1 Token

The following example would help you to validate or generate the `X-API-TOKEN`. The token generation rule refers to the [Seamless Wallet 1.1 Doc](https://github.com/jacky5823a/docs/blob/master/SeamlessWalletAPI1.x/SeamlessWallet1.1.md)

#### Running in Node.js

##### Validate X-API-TOKEN

```javascript
const { SeamlessWalletToken } = require('@token-library/js-token');

let key = '<your agent ID>';
let secret = '<your agent key>';
let seamlessWalletToken = new SeamlessWalletToken(key, secret);

let payload = '{"requestId":"test-123","user":"member"}';
let token = 'the-X-API-TOKEN-form-header';
seamlessWalletToken.validate(payload, token);
```

##### Generate X-API-TOKEN

```javascript
const { SeamlessWalletToken } = require('@token-library/js-token');

const key = '<your agent ID>';
const secret = '<your agent key>';
const seamlessWalletToken = new SeamlessWalletToken(key, secret);

const payload = '{"requestId":"test-123","status":"ok","user":"member","currency":"USD","balance":100}';

// The reformatPayload method would help you to reformat payload to match the rules, for example: reformat the number to 4 decimal places number
// So the payload after reformatting would be '{"balance":100.0000,"currency":"USD","requestId":"test-123","status":"ok","user":"member"}'
const reformatPayload = seamlessWalletToken.reformatPayload(payload);

const token = seamlessWalletToken.generateToken(reformatPayload);

// In your routes...
response.writeHead(200, {
    'Content-Type': 'application/json',
    'X-API-KEY': key,
    'X-API-TOKEN': token,
});
response.end(reformatPayload);
```

### Api Key(For keno-api)

The following example would help you to generate the `Key` in request parameters. The token generation rule refers to the [Encryption Flow](https://github.com/jacky5823a/docs/blob/master/AccountingPlatformAPI/encryption-en.md)

#### Running in Node.js

##### Generate key

```javascript
const { ApiToken } = require('@token-library/js-token');

const key = '<your agent ID>';
const secret = '<your agent key>';
const apiToken = new ApiToken(key, secret);

const params = {
    'AgentId': key,
    'Account': 'member',
    'LimitStake': '1,2,3'
};
const payload = JSON.stringify(params);
params.Key = apiToken.generateKey(payload);

// Send request
axios.post('api-url', params);
```

### HMAC Signature(For v2 api)
The following example would help you to generate the `Signature` in the request header.

#### Running in Node.js

##### Query string(GET)
```javascript
const axios = require('axios');

const { ApiV2Signature } = require('@token-library/js-token');

const agentId = '<your agent ID>';
const agentKey = '<your agent key>';
const apiV2Signature = new ApiV2Signature(agentId, agentKey);

const params = {
    'account': 'member',
    'currency': 'USD',
    'tableLimitIds': [88, 89, 90]
};
const payload = new URLSearchParams(params).toString();
const requestTimestamp = Math.floor(Date.now() / 1000);
const signature = apiV2Signature.generateSignature(payload, requestTimestamp);

// Send request
await axios.get('api-url', new URLSearchParams(params), {
    headers: {
        'Content-Type': 'application/json',
        'X-Agent-Id': agentId,
        'X-Agent-Timestamp': requestTimestamp,
        'X-Agent-Signature': signature,
    }
});
```

##### Request Body(POST, PATCH)
```javascript
const axios = require('axios');

const { ApiV2Signature } = require('@token-library/js-token');

const agentId = '<your agent ID>';
const agentKey = '<your agent key>';
const apiV2Signature = new ApiV2Signature(agentId, agentKey);

const params = {
    'account': 'member',
    'currency': 'USD',
    'tableLimitIds': [88, 89, 90]
};
const payload = JSON.stringify(params);
const requestTimestamp = Math.floor(Date.now() / 1000);
const signature = apiV2Signature.generateSignature(payload, requestTimestamp);

// Send request
await axios.post('api-url', params, {
    headers: {
        'Content-Type': 'application/json',
        'X-Agent-Id': agentId,
        'X-Agent-Timestamp': requestTimestamp,
        'X-Agent-Signature': signature,
    }
});
```

## Tests

```
yarn
yarn test
```

## License

Please see [MIT License File](./LICENSE) for more information.
